module Util where

-- The smallest multiple of `multiplier` which is >= `value`.
nextMultiple :: (Integral m, Integral v) => m -> v -> v
nextMultiple multiplier value = factor * fromIntegral multiplier
  where
    factor = factor' + (if remainder == 0 then 0 else 1)
    (factor', remainder) = value `divMod` fromIntegral multiplier
