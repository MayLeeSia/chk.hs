{-# LANGUAGE OverloadedStrings #-}

--
-- Glossary
--
-- CHK - An encryption and encoding scheme for storing immutable data.
--
-- Data - The plaintext used to construct CHK.
--
-- Share - One complete unit of encrypted and FEC encoded data.
--
-- Segment - One piece of ciphertext used to construct CHK.  All segments
--     belonging to a CHK are the same size except the last one may be short.
--
-- k, required - The number of "primary" erasure-encoding outputs.  Equal to
--     the minimum number of erasure-encoding outputs needed to reconstruct
--     the erasure-encoding input.
--
-- n, total - The total number of erasure-encoding outputs.  Always greater
--     than or equal to required.
--
-- Block - One output resulting from erasure-encoding one segment using
--     required, total.  If necessary, the input segment is nul-padded so its
--     size is a multiple of required.
--
-- Plaintext Hash Tree - Not actually implemented by Tahoe so I'm not really
--     sure.  Probably something like a sha256d merkle tree where the leaves
--     are hashes of the plaintext corresponding to each ciphertext segment.
--     Since all shares for a CHK are derived from the same plaintext, every
--     share has the same plaintext hash tree.
--
-- Crypttext Hash Tree - A sha256d merkle tree where the leaves are hashes of
--     the ciphertext segments.  Since all shares for a CHK are derived from
--     the same ciphertext, every share has the same ciphertext hash tree.
--
-- Crypttext Root Hash - The hash at the root of Crypttext Hash Tree.
--
-- Block Hash Tree - A sha256d merkle tree where the leaves are hashes of the
--     blocks.  Since the erasure-encoding output is different for each share,
--     every share has a different block hash tree.
--
-- Share Hash Tree - A sha256d merkle tree where the leaves are the root
--     hashes of the block hash trees for all shares.
--
-- Share Hashes - A list of hashes from the Share Hash Tree which are required
--     to verify one block hash tree.  Each share includes the Share Hashes
--     required to verify the Block Hash Tree contained within that share.
--     Since every share contains a different Block Hash Tree, every share
--     contains a different list of Share Hashes.  Each Share Hash in this
--     list is accompanied by information about its position in the Share Hash
--     Tree though it may not be strictly required (since it could be inferred
--     from position in the list).
--
-- URI Extension - A collection of metadata describing the encryption and
--     encoding used to create the CHK and (largely) necessary for either
--     decoding or verifying the integrity of the contained data.

module CHK
  ( makeChkEncoder
  , chkEncrypt
  , chkCap
  ) where

import Data.List.Extra
  ( snoc
  )

import Data.IORef
  ( IORef
  , newIORef
  , readIORef
  , writeIORef
  , modifyIORef
  )

import qualified Data.Map as Map

import Data.ByteString.Base32
  ( encodeBase32Unpadded
  )

import qualified Data.ByteString as B
import qualified Data.ByteString.Builder as B
import Data.ByteString.Lazy
  ( toStrict
  )
import qualified Data.ByteArray as BA

import qualified Data.Text as T
import Data.Text.Encoding
  ( encodeUtf8
  )

import Data.Serialize
  ( encode
  )
import qualified Codec.FEC as FEC

import Crypto.Types
  ( IV
  )
import Crypto.Classes
  ( zeroIV
  )

import Crypto.Cipher.AES128
  ( AESKey128
  , ctr
  )

import Crypto.Hash
  ( Context
  , SHA256
  , hashInit
  , hashUpdate
  , hashFinalize
  )

import Netstring
  ( netstring
  )

import Crypto
  ( blockHash
  , ciphertextTag
  , ciphertextSegmentHash
  , uriExtensionHash
  , sha256
  )

import Merkle
  ( MerkleTree
  , makeTreePartial
  , breadthFirstList
  , neededHashes
  )

import URIExtension
  ( URIExtension(..)
  , uriExtensionToBytes
  )

import Util
  ( nextMultiple
  )

import Types


-- Construct a URI:CHK: capability string.
chkCap :: Total -> Required -> Size -> URIExtension -> AESKey128 -> Capability
chkCap total required size uriExt key =
  Capability . T.intercalate ":" $
  [ "URI"
  , "CHK"
  , T.toLower . encodeBase32Unpadded $ encode key
  , T.toLower . encodeBase32Unpadded $ uriExtensionHash uriExt
  , T.pack . show $ required
  , T.pack . show $ total
  , T.pack . show $ size
  ]

data CHKState = CHKState
  {
    -- Indicate which area of the CHK data will be emitted next
    chkStateStage :: CHKStage

    -- During the ShareBlock stage, keeps a running list of hashes of each
    -- crypttext segment encoded so far.  During later stages this is used
    -- emit the "crypttext_root_hash" field of the URI extension and the value
    -- for the CrypttextHashTree stage.
  , chkStateCrypttextHashes :: [CrypttextHash]

    -- During the ShareBlock stage, builds a single hash of all crypttext
    -- segments encoded so far.  During later stages this is used to emit the
    -- "crypttext_hash" field of the URI extension.
  , chkStateCrypttextHash :: Context SHA256

    -- Assembled during the ShareBlock stage, keeps a running list of hashes
    -- of blocks, keyed by share number.  During later stages this is used to
    -- emit integrity information.
  , chkStateBlockHashes :: BlockHashMap
  }

-- CHKStage is internal state for the CHK encoder.  It keeps track of how far
-- through the encoding process the encoder has progressed.  For the version 1
-- CHK encoder, sizes and offsets are represented as 4 byte big-endian values.
-- For version the 2 CHK encoder, sizes and offsets are represented as 8 byte
-- big-endian values.  The stages are the same for both versions though.  At
-- each stage, the encoder needs to emit the corresponding piece of data and
-- then transition to the next stage.  Offsets are relative to the beginning
-- of the emitted data.  The values emitted by the encoder are constant across
-- all FEC shares except where noted.
data CHKStage
  = Version                  -- schema version for the emitted data
  | BlockSize                -- FEC block size - legacy, unused
  | ShareDataSize            -- share data length - legacy, unused
  | ShareDataOffset          -- offset of FEC share blocks
  | PlaintextHashTreeOffset  -- offset of plaintext hash tree - unimplemented
  | CrypttextHashTreeOffset  -- offset of crypttext hash tree
  | BlockHashesOffset        -- offset of block hash tree
  | ShareHashesOffset        -- offset of share hashes
  | URIExtensionOffset       -- offset ofURI extension
  | ShareBlock SegmentNum    -- A block of share data.  The segment number
                             -- indicates which source segment needs to be
                             -- encoded to produce the block of data.  These
                             -- vary from share to share.
  | PlaintextHashTree        -- Plaintext segment hashes - unimplemented
  | CrypttextHashTree        -- Ciphertext segment hashes
  | BlockHashTree            -- SHA256d hashes of the share blocks emitted
                             -- earlier.  The hashes are all concatenated to
                             -- form the BlockHashes. As blocks vary from
                             -- share to share, so do the block hashes.
  | ShareHashes              -- The Merkle tree hashes, excluding the root
                             -- hash, and their index, required to validate
                             -- the block hashes.  The root hash is in the
                             -- URIExtension.
  | URIExtensionLength       -- URI extension length
  | URIExtension'            -- URI extension bytes of the indicated length
  | Finished                 -- The terminal state.  There is no further data
                             -- to emit.
  deriving (Show, Eq, Ord)


-- Make an encoder to CHK encode an encrypted byte stream, producing N
-- different share data streams which can be used to write N different shares.
--
-- This replaces allmydata.immutable.layout and allmydata.immutable.encode
makeChkEncoder ::
  Parameters ->
  -- ^ The ZFEC parameters for this encoding.  This determines how many shares
  -- will come out of this function.
  Size ->
  -- ^ Total application data size that can be read from the function that
  -- comes next.
  (Integer -> IO B.ByteString) ->
  -- ^ A function to read the data to encode.  Normally should produce
  -- ciphertext.
  IO (IO (Either [B.ByteString] (AESKey128 -> Capability)))
  -- ^ An IO which can be evaluated to get the encoder.  The encoder is itself
  -- an IO which can be evaluated repeatedly to get the next chunk of data to
  -- append to the share data (Left) or the capability for the data (Right).
  -- Each time it evaluates to Left, each element of the list is next block of
  -- data for each of the shares.  The number of elements will be equal to the
  -- total value from the given Parameters (ie, one block for each share).
  -- When it evaluates to Right the data has been completely encoded.
makeChkEncoder param@(Parameters segmentSize total happy required) dataSize read = do
  state <- newIORef initialState
  return $ chkEncode version state param dataSize read
  where
    version =
      -- Tahoe also checks blockSize < 2 ^ 32 but I don't see how it is
      -- possible for blockSize to be greater than dataSize.
      if dataSize < 2 ^ 32
      then CHKv1
      else CHKv2
    initialState = CHKState
      { chkStateStage = Version
      , chkStateCrypttextHashes = mempty
      , chkStateCrypttextHash = crypttextContext
      , chkStateBlockHashes = mempty
      }

    crypttextContext = hashUpdate (hashInit :: Context SHA256) (netstring ciphertextTag)


-- Tahoe has two versions of CHK, one with 4 byte lengths/offsets and one with
-- 8 byte lengths/offsets.
data CHKVersion
  = CHKv1
  | CHKv2
  deriving (Show, Ord, Eq)


chkEncode ::
  CHKVersion ->
  -- ^ Which layout version should we emit
  IORef CHKState ->
  -- ^ Keep track of how far through encoding the encoder has progressed.
  Parameters ->
  -- ^ See makeChkEncoder
  Size ->
  -- ^ See makeChkEncoder
  (Integer -> IO B.ByteString) ->
  -- ^ See makeChkEncoder
  IO (Either [B.ByteString] (AESKey128 -> Capability))
  -- ^ See makeChkEncoder
chkEncode version stateRef p@(Parameters segmentSize total happy required) dataSize readEncrypted = do

  let intBE = case version of
        CHKv1 -> B.int32BE . fromIntegral
        CHKv2 -> B.int64BE . fromIntegral

  (CHKState currentStage crypttextHashes crypttextHashContext blockHashes) <- readIORef stateRef
  case currentStage of
    Version -> do
      -- print "chk_encode"
      -- print ("Data size:", dataSize)
      -- print ("Share size:", shareSize)
      -- print ("Block size:", blockSize)
      -- print ("Segment size:", segmentSize)
      -- print ("Num segments:", numSegments)
      -- print ("Effective segments:", effectiveSegments)
      -- print ("Segment hash size:", segmentHashSize)
      -- print ("Params:", p)
      -- print ("Tail params:", tailParams)
      -- print ("Tail padding:", tailPadding)

      setStage BlockSize
      same_blocks $
        case version of
          CHKv1 -> "\x00\x00\x00\x01"
          CHKv2 -> "\x00\x00\x00\x02"
    BlockSize -> do
      setStage ShareDataSize
      same_blocks . toStrictByteString . intBE $ blockSize
    ShareDataSize -> do
      setStage ShareDataOffset
      same_blocks . toStrictByteString $ intBE shareSize
    ShareDataOffset -> do
      setStage PlaintextHashTreeOffset
      same_blocks . toStrictByteString $ intBE 44
    PlaintextHashTreeOffset -> do
      setStage CrypttextHashTreeOffset
      same_blocks . toStrictByteString $ intBE (44 + shareSize)
    CrypttextHashTreeOffset -> do
      setStage BlockHashesOffset
      same_blocks . toStrictByteString $ intBE (44 + shareSize + 1 * segmentHashSize)
    BlockHashesOffset -> do
      setStage ShareHashesOffset
      same_blocks . toStrictByteString $ intBE (44 + shareSize + 2 * segmentHashSize)
    ShareHashesOffset -> do
      setStage URIExtensionOffset
      same_blocks . toStrictByteString $ intBE (44 + shareSize + 3 * segmentHashSize)
    URIExtensionOffset -> do
      setStage (ShareBlock 0)
      same_blocks . toStrictByteString $ intBE (44 + shareSize + 3 * segmentHashSize + shareHashTreeSize)
    ShareBlock segmentNumber -> do
      setStage $
        if segmentNumber + 1 == numSegments
        -- After reading this segment, we will have reached the expected (and
        -- published) share size.  Proceed.
        then PlaintextHashTree
        -- There are more segments to go after this one.  Remain in this stage
        -- but update the segment number.
        else ShareBlock (segmentNumber + 1)

      let paddedCiphertext crypttext =
            if segmentNumber + 1 == numSegments
            -- Allow the last ciphertext segment to be short.  Pad it with
            -- nulls if necessary.
            then padTailSegment crypttext
            else crypttext

      -- Read the ciphertext segment so we can update the crypttext hash.
      crypttext <- readEncrypted segmentSize
      crypttextHashUpdate crypttext

      -- print ("Segment number:", segmentNumber)

      -- Check!  rev 06d2f6347395f0e769a6a5dd4d15d38a2b86b9a7 this matches
      -- Tahoe for 64 byte input
      -- print ("Crypttext", B.length crypttext, encodeBase32Unpadded crypttext)

      -- print ("Crypttext segment hash", encodeBase32Unpadded $ ciphertextSegmentHash crypttext)

      -- Produce the FEC blocks for this piece of ciphertext.  Note the
      -- padding is only accounted for here, after the necessary crypttext
      -- hashes have been computed.
      let blocks = encodeSegment (paddedCiphertext crypttext)

      -- print ("Blocks, segment", segmentNumber, map encodeBase32Unpadded blocks)

      -- Hash the new blocks and capture the result in our state
      addBlockHashes (map blockHash blocks)

      different_blocks blocks
    PlaintextHashTree -> do -- XXX Unused by tahoe so we don't even try right now
      setStage CrypttextHashTree
      same_blocks $ B.replicate (fromIntegral segmentHashSize) 0
    CrypttextHashTree -> do
      setStage BlockHashTree
      let tree = makeTreePartial crypttextHashes
      -- print ("CrypttextHashTree inputs", map encodeBase32Unpadded crypttextHashes)
      -- print ("CrypttextHashTree outputs", map encodeBase32Unpadded . breadthFirstList $ tree)
      same_blocks . serializeTree $ tree
    BlockHashTree -> do
      setStage ShareHashes
      let trees = map serializeTree $ makeBlockHashTrees blockHashes
      -- print ("BlockHashes", map encodeBase32Unpadded trees)
      different_blocks trees
    ShareHashes -> do
      setStage URIExtensionLength
      let shareTree = makeShareTree . makeBlockHashTrees $ blockHashes
      let needed = [ neededHashes shareTree sharenum
                   | sharenum <- [0..total-1]
                   ]
      let serialized = map serializeNeededShares needed
      -- print ("Share hash lengths:", map B.length serialized)
      different_blocks serialized
    URIExtensionLength -> do
      setStage URIExtension'
      let uriExtBytes = makeUEB crypttextHashContext crypttextHashes blockHashes
      same_blocks . toStrictByteString . intBE . fromIntegral $ B.length uriExtBytes
    URIExtension' -> do
      setStage Finished
      let uriExtBytes = makeUEB crypttextHashContext crypttextHashes blockHashes
      same_blocks uriExtBytes
    Finished -> do
      let uriExt = makeUE crypttextHashContext crypttextHashes blockHashes
      -- print "URI extension is:"
      -- print uriExt
      finished $ chkCap total required dataSize uriExt
  where
    -- Alter the CHKState
    setStage newStage = modifyIORef stateRef $ \state -> state { chkStateStage = newStage }
    addBlockHashes :: [B.ByteString] -> IO ()
    addBlockHashes hashesToAdd =
      modifyIORef stateRef $ \state -> state { chkStateBlockHashes = newBlockHashes state }
      where
        newBlockHashes CHKState { chkStateBlockHashes = blockHashes } =
          combineBlockHashes blockHashes (zip [0..] hashesToAdd)

        combineBlockHashes :: BlockHashMap -> [(ShareNum, BlockHash)] -> BlockHashMap
        combineBlockHashes blockHashes [] = blockHashes
        combineBlockHashes blockHashes ((num, hash):rest) =
          Map.alter (Just . appendHash hash) num (combineBlockHashes blockHashes rest)

        -- TODO Consider reversing the order of this list to avoid the appends here
        appendHash newHash Nothing = [newHash]
        appendHash newHash (Just hashes) = snoc hashes newHash

    -- Update the crypttext segment hashes and crypttext hash state with the
    -- given segment of crypttext.
    crypttextHashUpdate :: B.ByteString -> IO ()
    crypttextHashUpdate crypttext =
      modifyIORef stateRef $ \state -> state
      { chkStateCrypttextHash = hashUpdate (chkStateCrypttextHash state) crypttext
      , chkStateCrypttextHashes = snoc (chkStateCrypttextHashes state) (ciphertextSegmentHash crypttext)
      }

    -- Return a single value for all shares
    same_blocks = return . Left . replicate (fromIntegral total)

    -- Return a different value for each share
    different_blocks = return . Left

    -- Encoding is done, return a function to compute the capability.
    finished = return . Right

    toStrictByteString = toStrict . B.toLazyByteString

    -- The total number of segments we expect to handle.
    -- allmydata.immutable.encode.Encoder._got_all_encoding_parameters
    numSegments = ceiling (fromIntegral dataSize / fromIntegral segmentSize)

    -- The size in bytes of one erasure-encoded block of data.
    -- allmydata.immutable.encode.Encoder._got_all_encoding_parameters +
    -- allmydata.codec.CRSEncoder.set_params
    blockSize = ceiling $ fromIntegral segmentSize / fromIntegral required

    -- CRSEncoder.set_params
    shareSize = ceiling (fromIntegral dataSize / fromIntegral required)

    -- Return the smallest integer which is a power of k and greater than or
    -- equal to n
    nextPowerOf k n =
      nextPowerOf' k n 1
      where
        nextPowerOf' k n p =
          if p < n
          then nextPowerOf' k n (p * k)
          else p

    -- allmydata.immutable.layout.WriteBucketProxy
    hashSize = 32
    effectiveSegments = nextPowerOf 2 numSegments
    segmentHashSize = fromIntegral $ (2 * effectiveSegments - 1) * fromIntegral hashSize

    -- The number of Share Hashes required to validate each share's Block Hash
    -- Tree (and therefore the number of Shares Hashes included in each
    -- share).  The contents of the tree are irrelevant since we're only using
    -- this to compute the number of Share Hashes.  We can select an arbitrary
    -- share number to ask about here because all shares will require the same
    -- *number* of Share Hashes.
    numShareHashes = length $ neededHashes (makeTreePartial $ replicate numSegments "") 0
    -- The number of bytes Share Hashes will require in the share.  Though
    -- this has "tree" in its name the hashes do not form a complete tree,
    -- though they are taken from the Share Hash Tree.  The naming agrees with
    -- Tahoe's at the moment.  The two extra bytes are for the share num
    -- attached to each hash (which may well be redundant).
    shareHashTreeSize = numShareHashes * (2 + hashSize)

    makeBlockHashTrees :: BlockHashMap -> [MerkleTree]
    makeBlockHashTrees = map (makeTreePartial . snd) . Map.toAscList

    serializeTree :: MerkleTree -> B.ByteString
    serializeTree = B.concat . breadthFirstList

    makeCrypttextHash :: Context SHA256 -> CrypttextHash
    makeCrypttextHash = sha256 . toBytes . hashFinalize
      where
        toBytes = B.pack . BA.unpack

    makeCrypttextRootHash :: [CrypttextHash] -> CrypttextHash
    makeCrypttextRootHash = head . breadthFirstList . makeTreePartial

    makeShareRootHash :: BlockHashMap -> CrypttextHash
    makeShareRootHash = head . breadthFirstList . makeShareTree . makeBlockHashTrees

    makeShareTree :: [MerkleTree] -> MerkleTree
    makeShareTree = makeTreePartial . map (head . breadthFirstList)

    serializeNeededShares :: [(ShareNum, B.ByteString)] -> B.ByteString
    serializeNeededShares = B.concat . pieces
      where
        pieces [] = []
        pieces ((sharenum, hash):xs) =
          (toStrictByteString . B.int16BE . fromIntegral $ sharenum):hash:pieces xs

    --
    -- Parameters stuff
    --

    makeUEB a b c = uriExtensionToBytes $ makeUE a b c

    makeUE :: Context SHA256 -> [BlockHash] -> BlockHashMap -> URIExtension
    makeUE crypttextHashContext crypttextHashes blockHashes =
      uriExt
      where
        uriExt = uriExtension crypttextHash crypttextRootHash shareRootHash
        crypttextHash = makeCrypttextHash crypttextHashContext
        crypttextRootHash = makeCrypttextRootHash crypttextHashes
        shareRootHash = makeShareRootHash blockHashes

    uriExtension :: CrypttextHash -> CrypttextHash -> CrypttextHash -> URIExtension
    uriExtension crypttextHash crypttextRootHash shareRootHash =
      URIExtension
      { uriExtCodecName = "crs"
      , uriExtCodecParams = p
      , uriExtSize = dataSize
      , uriExtSegmentSize = segmentSize
      , uriExtNeededShares = required
      , uriExtTotalShares = total
      , uriExtNumSegments = numSegments
      , uriExtTailCodecParams = tailParams

      , uriExtCrypttextHash = crypttextHash
      , uriExtCrypttextRootHash = crypttextRootHash
      , uriExtShareRootHash = shareRootHash
      }

    -- Construct the encoding parameters for the final segment which may be
    -- smaller than the earlier segments (if the size of the data to be
    -- encoded is not a multiple of the segment size).
    -- allmydata.immutable.encode.Encoder._got_all_encoding_parameters
    tailParams = Parameters tailSegmentSize total happy required
      where
        tailSegmentSize = nextMultiple required tailSize'
        tailSize' =
          if tailSize == 0
          then segmentSize
          else tailSize
        tailSize = fromIntegral dataSize `mod` segmentSize

    -- The padding that allmydata.immutable.upload.Encoder._gather_data does
    -- for short reads when allow_short=True
    padTailSegment segment = B.concat [ segment, tailPadding ]

    -- The right amount of nul padding for the tail segment.
    tailPadding = B.replicate (fromIntegral paddingSize) 0x00
      where
        -- How many nul bytes do we need to add to the tail data to make a
        -- tail segment?
        paddingSize =
          if tailDataSize == 0
          -- There is no tail segment.  It's just the last segment.
          then 0
          -- The last segment is a tail segment and needs padding.
          else tailSegmentSize - tailDataSize
        -- How long did we compute the tail segment needs to be?
        (Parameters tailSegmentSize _ _ _) = tailParams
        -- How much application data does the tail segment carry?
        tailDataSize = dataSize `mod` segmentSize

    encodeSegment segment = pieces ++ FEC.encode p pieces
      where
        pieces = slice segment

        p = FEC.fec (fromIntegral required) (fromIntegral total)
        size = B.length segment `div` fromIntegral required
        slice :: B.ByteString -> [B.ByteString]
        slice "" = []
        slice xs =
          let (piece, rest) = B.splitAt size xs
          in piece:slice rest

-- AES-CTR encrypt a cleartext data stream to produce a ciphertext data stream.
--
-- This replaces allmydata.immutable.upload.EncryptAnUploadable
chkEncrypt :: AESKey128 -> (Integer -> IO B.ByteString) -> IO (Integer -> IO B.ByteString)
chkEncrypt key readCleartext = do
  -- ctr only works "correctly" (for our purposes) on whole blocks.  If a
  -- partial block is requested then we need to read the whole block it is
  -- part of and then save some of the output for the next call.
  --
  -- We also need to save the updated nonce (called an IV here mistakenly) for
  -- use with the next call.
  --
  -- Here are those two values.
  ref <- newIORef (zeroIV, "")
  return $ read ref
    where
      read :: IORef (IV AESKey128, B.ByteString) -> Integer -> IO B.ByteString
      read ref requestedLength = do
        (iv, lastTime) <- readIORef ref

        -- If we already had enough buffered, we're just done.
        if fromIntegral requestedLength <= B.length lastTime
          then
          do
            let (thisTime, nextTime) = B.splitAt (fromIntegral requestedLength) lastTime
            writeIORef ref (iv, nextTime)
            return thisTime
          else
          do
            -- How many more bytes do we need to acquire so that in combination
            -- with whatever is buffered, we produce the requested amount of data?
            let newLength = fromIntegral requestedLength - B.length lastTime

            -- How many bytes do we actually have to read in order to obtain at
            -- least the number of additional bytes we need?
            let blockSafeLength = nextMultiple blockSize newLength

            -- Get the amount of cleartext we have to read to produce at least the
            -- amount of ciphertext we need.
            cleartext <- readCleartext (fromIntegral blockSafeLength)
            let (ciphertext, newIV) = ctr key iv cleartext

            -- Split the ciphertext into the part we need to return from this call
            -- and the part we're saving for next time.
            let (thisTime, nextTime) = B.splitAt newLength ciphertext

            -- Update our internal state so we can do the next read correctly
            writeIORef ref (newIV, nextTime)

            -- Give back any buffered ciphertext plus the extra ciphertext we
            -- needed to produce this time.
            return $ B.concat [ lastTime, thisTime ]

      blockSize = 16
