{-# LANGUAGE OverloadedStrings #-}

module Merkle
  ( MerkleTree(MerkleNode, MerkleLeaf)
  , makeTree
  , makeTreePartial
  , breadthFirstList
  , neededHashes

  , rootHash
  , pairHash
  , emptyLeafHash
  ) where

import Crypto.Hash
  ( HashAlgorithm
  )

import qualified Data.ByteString as B

import qualified Data.Text as T
import Data.Text
  ( Text
  , pack
  )
import Data.Text.Encoding
  ( encodeUtf8
  , decodeUtf8
  )

import Data.ByteString.Base32
  ( encodeBase32Unpadded
  )

import Crypto
  ( taggedHash
  , taggedPairHash
  )

data MerkleTree
  = MerkleLeaf B.ByteString
  | MerkleNode B.ByteString MerkleTree MerkleTree
  deriving (Eq)

instance Show MerkleTree where
  show (MerkleLeaf value) =
    T.unpack $ T.concat [ "MerkleLeaf ", encodeBase32Unpadded value ]
  show (MerkleNode value left right) =
    T.unpack $ T.concat
    [ "MerkleNode " :: T.Text, encodeBase32Unpadded value
    , " (", T.pack $ show left, ")"
    , " (", T.pack $ show right, ")"
    ]

emptyLeafHash :: Int -> B.ByteString
emptyLeafHash = taggedHash 32 "Merkle tree empty leaf" . encodeUtf8 . pack . show

pairHash :: B.ByteString -> B.ByteString -> B.ByteString
pairHash = taggedPairHash 32 "Merkle tree internal node"

rootHash :: MerkleTree -> B.ByteString
rootHash (MerkleLeaf value) = value
rootHash (MerkleNode value _ _) = value

-- Like makeTree but error on empty list
makeTreePartial :: [B.ByteString] -> MerkleTree
makeTreePartial = unJust . makeTree
  where
    unJust Nothing = error "Merkle.makeTreePartial failed to make a tree"
    unJust (Just t) = t

-- Make a merkle tree for the given values.  Extra values are generated to
-- fill the tree if necessary.  The given values are the values of the leaf
-- nodes.
makeTree :: [B.ByteString] -> Maybe MerkleTree
makeTree [] = Nothing
makeTree leaves =
  Just $ makeTree' (pad leaves)
  where
    -- Pad the leaves out to the next power of two so the tree is full.
    pad :: [B.ByteString] -> [B.ByteString]
    pad leaves = leaves ++ padding (length leaves)

    -- Create the padding for the pad function
    padding :: Int -> [B.ByteString]
    padding size = [ emptyLeafHash n | n <- [size..2 ^ ceiling (logBase 2 (fromIntegral size)) - 1] ]

    -- Turn a length-of-power-of-2 list into a tree
    makeTree' :: [B.ByteString] -> MerkleTree
    makeTree' [x] = MerkleLeaf x
    makeTree' xs =
      makeNode (makeTree' left) (makeTree' right)
      where
        (left, right) = splitAt (length xs `div` 2) xs

    -- Make a parent node referencing two given child nodes, calculating the
    -- parent node's hash in the process.
    makeNode :: MerkleTree -> MerkleTree -> MerkleTree
    makeNode left right =
      MerkleNode (pairHash (rootHash left) (rootHash right)) left right

-- Convert a tree to a breadth-first list of its hash values.
breadthFirstList :: MerkleTree -> [B.ByteString]
breadthFirstList tree = traverse [tree]
  where
    traverse :: [MerkleTree] -> [B.ByteString]
    traverse [] = []
    traverse trees =
      [ rootHash tree | tree <- trees ] ++ traverse (concat [ children tree | tree <- trees ])

    children (MerkleLeaf _) = []
    children (MerkleNode _ left right) = [left, right]

-- Count the number of nodes in a tree.
size :: MerkleTree -> Int
size (MerkleLeaf _) = 1
size (MerkleNode _ left right) = 1 + size left + size right

-- Determine the smallest index into the breadth first list for the given tree
-- where a leaf may be found.
firstLeafNum :: MerkleTree -> Int
firstLeafNum tree = size tree `div` 2

-- Given the number of a leaf in a tree and the tree, determine which hashes
-- are required to validate the data that leaf corresponds to.
--
-- Suppose you have the segment of ciphertext corresponding to the hash at
-- leaf position N.  You have also obtain the root hash of the tree in a
-- secure way.  What more do you need to know in order to verify that the
-- ciphertext you have is the same ciphertext originally used to construct the
-- tree?
--
-- Start by hashing the segment you have.  Now you need the hash of the
-- ciphertext segment that is a sibling of the one you have.  Once you have
-- that you can construct the hash at your segment's parent node yourself.  If
-- that parent node is not the root, you need the hash from its sibling node.
-- That lets you construct the hash at the next parent.  Continue in this
-- manner until you reach the root.  Once there, you can compare your computed
-- root hash against the root hash you obtained some other way.  If and only
-- if they match, your ciphertext is correct.
--
-- The whole process involves knowing (retrieving) only about log2(total
-- segments) hashes.
--
-- The more of the segments you already have (eg because you downloaded them
-- because you want them), the fewer hashes you need to request because you
-- can (and must) compute them locally.
--
-- This version of the function doesn't consider what information might
-- already be locally available.  It just returns all of the positions in the
-- tree where a hash is needed from *somewhere*.
neededHashes :: Integral i => MerkleTree -> i -> [(i, B.ByteString)]
neededHashes tree leafNum =
  -- XXX Pretty inefficient
  zip (map fromIntegral neededIndexes) $ map ((!!) $ breadthFirstList tree) neededIndexes
  where
    neededIndexes = nodeIndex:neededFor nodeIndex

    nodeIndex = firstLeafNum tree + fromIntegral leafNum

    neededFor 0 = []
    neededFor nodeIndex = sibling nodeIndex:neededFor (parent nodeIndex)

    sibling index =
      if (leftChild . parent) index == index
      then (rightChild . parent) index
      else (leftChild . parent) index

    parent index = (index - 1) `div` 2
    leftChild index = 2 * index + 1
    rightChild index = 2 * index + 2
